CC=gcc
CFLAGS=-I.

SRC_DIR=./src
BUILD_DIR=./build
FIG_DIR=./figs

#
# Utility functions
#
image: ${SRC_DIR}/image.c ${SRC_DIR}/image.h ${BUILD_DIR}
	$(CC) -c ${SRC_DIR}/image.c -o ${BUILD_DIR}/image.o

utility: ${SRC_DIR}/utility.c ${SRC_DIR}/utility.h ${BUILD_DIR}
	$(CC) -c ${SRC_DIR}/utility.c -o ${BUILD_DIR}/utility.o


#
# Main file
#
main: ${SRC_DIR}/main.c ${SRC_DIR}/utility.h ${SRC_DIR}/image.h ${BUILD_DIR}
	$(CC) -c ${SRC_DIR}/main.c -o ${BUILD_DIR}/main.o

#
# Link .o
#
image_proc: main image utility ${BUILD_DIR} ${FIG_DIR}
	$(CC) ${BUILD_DIR}/main.o ${BUILD_DIR}/utility.o ${BUILD_DIR}/image.o -o ./image_proc

#
# mkdir if the directory does not exist
#
${BUILD_DIR}:
	@mkdir -p ${BUILD_DIR}

${FIG_DIR}:
	@mkdir -p ${FIG_DIR}

#
# Clean all compiled files
#
.PHONY: clean
clean:
	rm -f $(BUILD_DIR)/* && rm -f ./image_proc