#include "image.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../stb_image/stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../stb_image/stb_image_write.h"

void loadImage(Image* img, const char* imgPath) {
    img->data = stbi_load(imgPath, &img->width, &img->height, &img->channels,
        STBI_rgb_alpha);

    if (img->data == NULL) {
        printf("ERROR: error in loading image.");
        exit(1);
    }

    img->size = img->width * img->height * img->channels;
    img->allocationType = STB_ALLOCATED;
}

void allocateImageMemory(Image* img, int width, int height, int channels) {
    /* Allocate momory for data. */
    size_t size = width * height * channels;
    img->data = malloc(size);

    /* Information of the image. */
    if (img->data != NULL) {
        img->size = size;
        img->width = width;
        img->height = height;
        img->channels = channels;
        img->allocationType = SELF_ALLOCATED;
    }
}

void freeImage(Image* img) {
    if (img->data != NULL) {
        if (img->allocationType == STB_ALLOCATED) {
            stbi_image_free(img->data);

        }
        else {
            free(img->data);
        }
        img->data = NULL;
        img->width = 0;
        img->height = 0;
        img->size = 0;
    }
}

void printRGB(Image* img) {
    /* */
    int i, j;
    unsigned bytePerPixel = img->channels;
    unsigned char* pixelOffset;
    /* Iterate over pixels. */
    for (j = 0; j < img->height; j++) {
        for (i = 0; i < img->width; i++) {
            pixelOffset = img->data + (i + img->width * j) * bytePerPixel;
            unsigned char r = pixelOffset[0];
            unsigned char g = pixelOffset[1];
            unsigned char b = pixelOffset[2];
            unsigned char a = img->channels >= 4 ? pixelOffset[3] : 0xff;
            printf("pixel[%i][%i]: rgb(%i, %i, %i), a:%i\n", i, j, r, g, b, a);

            if ((i + img->width * j) > 100) {
                return;
            }
        }
    }
}

void saveImage(const Image* img, const char* Path) {
    /* Save as a new figure.*/
    if (checkFileFormat(Path, ".png") || checkFileFormat(Path, ".PNG")) {
        stbi_write_png(Path, img->width, img->height, img->channels, img->data,
            img->width * img->channels);
    }
    else {
        printf("ERROR: Unsupported format. Currently only PNG is supported.");
    }
}

void copyData(const Image* copyFrom, Image* copyTo) {
    /* Allocate memory. */
    allocateImageMemory(copyTo, copyFrom->width, copyFrom->height,
        copyFrom->channels);

    int i, j;
    unsigned bytePerPixel = copyFrom->channels;
    unsigned char* pixelFrom, * pixelTo;
    /* Iterate over pixels. */
    for (j = 0; j < copyFrom->height; j++) {
        for (i = 0; i < copyFrom->width; i++) {
            pixelFrom =
                copyFrom->data + (i + copyFrom->width * j) * bytePerPixel;
            pixelTo = copyTo->data + (i + copyTo->width * j) * bytePerPixel;
            pixelTo[0] = pixelFrom[0];
            pixelTo[1] = pixelFrom[1];
            pixelTo[2] = pixelFrom[2];
            if (copyFrom->channels >= 4) {
                pixelTo[3] = pixelFrom[3];
            }
        }
    }
}

/*
Transformation functions.
*/
void convertToGreyScale(const Image* img, Image* greyImg) {
    if (!(img->channels >= 3)) {
        printf("ERROR: The input image must have at least 3 channels.");
        exit(1);
    }
    /* Declare constants. */
    int greyChannels = img->channels == 4 ? 2 : 1;
    allocateImageMemory(greyImg, img->width, img->height, greyChannels);
    if (greyImg->data == NULL) {
        printf("ERROR: Error allocating memory for new image.");
        exit(1);
    }

    /* Iterate over pixels and convert them to grey scale. */
    unsigned bytePerPixel = img->channels;
    unsigned bytePerGreyPixel = greyImg->channels;
    unsigned char* thisPixel, * thisGreyPixel;
    for (int i = 0; i < img->width; i++) {
        for (int j = 0; j < img->height; j++) {
            thisPixel = img->data + (i + img->width * j) * bytePerPixel;
            thisGreyPixel =
                greyImg->data + (i + greyImg->width * j) * bytePerGreyPixel;
            thisGreyPixel[0] =
                (thisPixel[0] + thisPixel[1] + thisPixel[2]) / 3.;
            if (img->channels == 4) {
                thisGreyPixel[1] = thisPixel[3];
            }
        }
    }
}

void blurImage(const Image* img, Image* blurredImg) {
    if (!(img->channels >= 3)) {
        printf("ERROR: The input image must have at least 3 channels.");
        exit(1);
    }
    /* Declare constants. */
    allocateImageMemory(blurredImg, img->width, img->height, img->channels);
    if (blurredImg->data == NULL) {
        printf("ERROR: Error allocating memory for new image.");
        exit(1);
    }

    // Size of the blurring box.
    int npad = 20;

    // Temporary variables.
    float averageBlue = 0.;
    float averageGreen = 0.;
    float averageRed = 0.;
    int count = 0;

    /* Iterate over pixels and convert them to grey scale. */
    unsigned bytePerPixel = img->channels;
    unsigned bytePerNewPixel = blurredImg->channels;
    unsigned char* thisPixel, * thisNewPixel;
    for (int i = 0; i < img->width; i++) {
        for (int j = 0; j < img->height; j++) {
            // Reset
            averageBlue = 0.;
            averageGreen = 0.;
            averageRed = 0.;
            count = 0;

            // Loop through the blurring box.
            for (int k = i - npad; k <= i + npad; k++) {
                // Handle boundaries.
                if (k < 0 || k >= img->height) {
                    continue;
                }

                for (int l = j - npad; l <= j + npad; l++) {
                    // Handle boundaries.
                    if (l < 0 || l >= img->width) {
                        continue;
                    }

                    thisPixel = img->data + (k + img->width * l) * bytePerPixel;
                    averageBlue += thisPixel[2];
                    averageGreen += thisPixel[1];
                    averageRed += thisPixel[0];
                    count += 1;
                }
            }

            thisNewPixel =
                blurredImg->data + (i + blurredImg->width * j) * bytePerNewPixel;
            thisNewPixel[0] = (int)averageRed / count;
            thisNewPixel[1] = (int)averageGreen / count;
            thisNewPixel[2] = (int)averageBlue / count;
            if (img->channels == 4) {
                thisNewPixel[3] = thisPixel[3];
            }
        }
    }
}

void getEdges(const Image* img, Image* edgedImg) {
    if (!(img->channels >= 3)) {
        printf("ERROR: The input image must have at least 3 channels.");
        exit(1);
    }
    /* Declare constants. */
    allocateImageMemory(edgedImg, img->width, img->height, img->channels);
    if (edgedImg->data == NULL) {
        printf("ERROR: Error allocating memory for new image.");
        exit(1);
    }

    // https://en.wikipedia.org/wiki/Sobel_operator
    int Gx[3][3] = { {-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1} };
    int Gy[3][3] = { {-1, -2, -1}, {0, 0, 0}, {1, 2, 1} };


    // Start computing the gradient.
    float gxBlue = 0., gyBlue = 0.;
    float gxGreen = 0., gyGreen = 0.;
    float gxRed = 0., gyRed = 0.;

    /* Iterate over pixels and convert them to grey scale. */
    unsigned bytePerPixel = img->channels;
    unsigned bytePerNewPixel = edgedImg->channels;
    unsigned char* thisPixel, * thisNewPixel;

    for (int i = 0; i < img->height; i++) {
        for (int j = 0; j < img->width; j++) {
            // Reset gradient for each pixel.
            gxBlue = 0., gyBlue = 0.;
            gxGreen = 0., gyGreen = 0.;
            gxRed = 0., gyRed = 0.;

            for (int k = 0; k < 3; k++) {
                int hIndex = i - 1 + k;
                // Handle boundary.
                if (hIndex < 0 || hIndex >= img->height) {
                    continue;
                }

                for (int l = 0; l < 3; l++) {
                    int wIndex = j - 1 + l;
                    // Handle boundary.
                    if (wIndex < 0 || wIndex >= img->width) {
                        continue;
                    }

                    // Sum up gradients.
                    thisPixel = img->data + (k + img->width * l) * bytePerPixel;
                    gxBlue += thisPixel[2] * Gx[k][l];
                    gyBlue += thisPixel[2] * Gy[k][l];
                    gxGreen += thisPixel[1] * Gx[k][l];
                    gyGreen += thisPixel[1] * Gy[k][l];
                    gxRed += thisPixel[0] * Gx[k][l];
                    gyRed += thisPixel[0] * Gy[k][l];
                }
            }

            // Update pixel.
            thisNewPixel =
                edgedImg->data + (i + edgedImg->width * j) * bytePerNewPixel;
            int sqrtBlue = (int)round(sqrtf(gxBlue * gxBlue + gyBlue * gyBlue));
            thisNewPixel[2] = sqrtBlue > 255 ? 255 : sqrtBlue < 0 ? 0 : sqrtBlue;

            int sqrtGreen = (int)round(sqrtf(gxGreen * gxGreen + gyGreen * gyGreen));
            thisNewPixel[1] = sqrtGreen > 255 ? 255 : sqrtGreen < 0 ? 0 : sqrtGreen;

            int sqrtRed = (int)round(sqrtf(gxRed * gxRed + gyRed * gyRed));
            thisNewPixel[0] = sqrtRed > 255 ? 255 : sqrtRed < 0 ? 0 : sqrtRed;

            // if (img->channels == 4) {
            //     thisNewPixel[3] = thisPixel[3];
            // }
        }
    }
}

void reflect(const Image* img, Image* reflectedImg) {
    if (!(img->channels >= 3)) {
        printf("ERROR: The input image must have at least 3 channels.");
        exit(1);
    }
    /* Declare constants. */
    allocateImageMemory(reflectedImg, img->width, img->height, img->channels);
    if (reflectedImg->data == NULL) {
        printf("ERROR: Error allocating memory for new image.");
        exit(1);
    }

    /* Iterate over pixels and convert them to grey scale. */
    unsigned bytePerPixel = img->channels;
    unsigned bytePerReflectedPixel = reflectedImg->channels;
    unsigned char* thisPixel, * thisReflectedPixel;
    int width = img->width, height = img->height;
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            thisPixel = img->data + ((width - 1 - i) + width * j) * bytePerPixel;
            thisReflectedPixel =
                reflectedImg->data + (i + width * j) * bytePerReflectedPixel;
            thisReflectedPixel[0] = thisPixel[0];
            thisReflectedPixel[1] = thisPixel[1];
            thisReflectedPixel[2] = thisPixel[2];
            if (bytePerPixel == 4) {
                thisReflectedPixel[3] = thisPixel[3];
            }
        }
    }
}

void flip(const Image* img, Image* flippedImg) {
    if (!(img->channels >= 3)) {
        printf("ERROR: The input image must have at least 3 channels.");
        exit(1);
    }
    /* Declare constants. */
    allocateImageMemory(flippedImg, img->width, img->height, img->channels);
    if (flippedImg->data == NULL) {
        printf("ERROR: Error allocating memory for new image.");
        exit(1);
    }

    /* Iterate over pixels and convert them to grey scale. */
    unsigned bytePerPixel = img->channels;
    unsigned bytePerFlippedPixel = flippedImg->channels;
    unsigned char* thisPixel, * thisFlippedPixel;
    int width = img->width, height = img->height;
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            thisPixel = img->data + (i + width * (height - 1 - j)) * bytePerPixel;
            thisFlippedPixel =
                flippedImg->data + (i + width * j) * bytePerFlippedPixel;
            thisFlippedPixel[0] = thisPixel[0];
            thisFlippedPixel[1] = thisPixel[1];
            thisFlippedPixel[2] = thisPixel[2];
            if (bytePerPixel == 4) {
                thisFlippedPixel[3] = thisPixel[3];
            }
        }
    }
}