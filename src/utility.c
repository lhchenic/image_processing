#include "utility.h"

bool checkPNG(const char* fname) {
    if (checkFileFormat(fname, ".png") || checkFileFormat(fname, ".PNG")) {
        return true;
    }
    else {
        return false;
    }
}

bool checkJPG(const char* fname) {
    if (checkFileFormat(fname, ".jpg") || checkFileFormat(fname, ".JPG") ||
        checkFileFormat(fname, ".jpeg") || checkFileFormat(fname, ".JPEG")) {
        return true;
    }
    else {
        return false;
    }
}

bool checkFileFormat(const char* fname, const char* fmt) {
    /* Get the last occurance of '.' */
    char* nameEnd = strrchr(fname, '.');
    if (!nameEnd) {
        printf("ERROR: File does not have an extension to indicate format.\n");
        exit(1);
    }
    return !strcmp(nameEnd, fmt);
}