#include "image.h"

int main(int argc, char* argv[]) {
    // Ensure proper usage.
    if (argc != optind + 2) {
        printf("Usage: ./image_proc -<ACTION> <ORIGINAL_FIGURE>\n");
        printf("Available actions:\n");
        printf("Convert colored image to grey scale: \n");
        printf("./image_proc -g original.png\n");
        printf("Blur image: \n");
        printf("./image_proc -b original.png\n");
        printf("Get edges of the image: \n");
        printf("./image_proc -e original.png\n");
        printf("Reflect image: \n");
        printf("./image_proc -r original.png\n");
        printf("Flip image upside down: \n");
        printf("./image_proc -f original.png\n");
        exit(1);
    }

    /* Get what action the user wishes to use. */
    const int LEN_ACTIONS = 5;
    const char ACTIONS[LEN_ACTIONS][3] = { "-g", "-b", "-e", "-r", "-f" };
    char* action = argv[optind];
    int actionIndex = -1;
    for (int i = 0; i < LEN_ACTIONS; i++) {
        if (strcmp(action, ACTIONS[i]) == 0) {
            actionIndex = i;
            break;
        }
    }
    if (actionIndex == -1) {
        printf("ERROR: Action not allowed.\n");
        exit(1);
    }

    /* Get the name of the original file and check format. */
    char* originalFile = argv[optind + 1];
    if (checkPNG(originalFile) == false) {
        printf("ERROR: Currently only PNG files are supported.");
        exit(1);
    }
    char originalPath[256];
    sprintf(originalPath, "%s%s", IMAGE_DIR, originalFile);

    /* Allocate memory for the original image and load its content. */
    Image originalImg;
    loadImage(&originalImg, originalPath);
    /* Allocate memory for grey image. */
    Image newImg;


    char newPath[256];
    char* end = strrchr(originalFile, '.');
    int nameLen = strlen(originalFile) - strlen(end);
    char newName[nameLen + 1];
    int i;
    for (i = 0; i < nameLen; i++) {
        newName[i] = originalFile[i];
    }
    newName[nameLen] = '\0';

    /* Convert to grey scale. */
    if (actionIndex == 0) {
        convertToGreyScale(&originalImg, &newImg);
        sprintf(newPath, "%s%s%s", IMAGE_DIR, newName, "_grey.png");
    }
    /* Blur image. */
    else if (actionIndex == 1) {
        blurImage(&originalImg, &newImg);
        sprintf(newPath, "%s%s%s", IMAGE_DIR, newName, "_blur.png");
    }
    /* Get edges. */
    else if (actionIndex == 2) {
        getEdges(&originalImg, &newImg);
        sprintf(newPath, "%s%s%s", IMAGE_DIR, newName, "_edge.png");
    }
    /* Reflect image. */
    else if (actionIndex == 3) {
        reflect(&originalImg, &newImg);
        sprintf(newPath, "%s%s%s", IMAGE_DIR, newName, "_reflect.png");
    }
    /* Flip image. */
    else if (actionIndex == 4) {
        flip(&originalImg, &newImg);
        sprintf(newPath, "%s%s%s", IMAGE_DIR, newName, "_flip.png");
    }

    /* Save to a new figure. */
    saveImage(&newImg, newPath);

    /* Free memory. */
    freeImage(&originalImg);
    freeImage(&newImg);
}