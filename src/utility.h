#include <getopt.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IMAGE_DIR "./figs/"

/* General utiilty */
bool checkFileFormat(const char* fname, const char* fmt);
bool checkJPG(const char* fname);
bool checkPNG(const char* fname);
