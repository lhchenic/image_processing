# Image Processing
### Author: [陳立馨 Li-Hsin Chen](https://www.linkedin.com/in/lhchen7180/) 

#### Description: This is a small application that allows one to process an image and perform some actions to it.

# Usage

## Compilation
After downloading the repository, you can compile the application using  
`make image_proc`  
which will create an executable `iamge_proc` in the root folder.

## Convert an image to grey scane
To convert a colored image to grey scale, run the following command after successful compilation of `image_proc`  
`./image_proc -g <NAME_OF_COLOR_FIGURE>`  
Make sure that the image is stored under the folder `figs`.  

# LICENSE
This project is distributed under the MIT License. See LICENSE for details.